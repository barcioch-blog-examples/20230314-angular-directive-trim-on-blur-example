import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styles: []
})
export class AppComponent {
  textAreaControl = new FormControl();
  inputControl = new FormControl();

  form = new FormGroup({
    input: new FormControl(),
    textarea: new FormControl(),
  });
}
