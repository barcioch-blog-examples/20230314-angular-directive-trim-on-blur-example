import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Component } from '@angular/core';
import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatInputHarness } from '@angular/material/input/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import { TrimOnBlurModule } from './trim-on-blur.module';

describe('TrimOnBlurModule', () => {
  let fixture: ComponentFixture<TestComponent>;
  let loader: HarnessLoader;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        CommonModule,
        NoopAnimationsModule,
        MatFormFieldModule,
        ReactiveFormsModule,
        MatInputModule,
        TrimOnBlurModule
      ],
      declarations: [TestComponent],
    });

    fixture = TestBed.createComponent(TestComponent);
    loader = TestbedHarnessEnvironment.loader(fixture);
  });

  describe('when component is initialized', () => {

    describe('test formControl', () => {
      describe('test textarea', () => {
        describe('and user sets "test me" value', () => {
          beforeEach(async () => {
            const input = await getTextareaHarness(formControlWrapperAncestor);
            await input.focus();
            await input.setValue('test me');
            await input.blur();
            fixture.detectChanges();
          });

          it('should not change control value', async () => {
            const input = await getTextareaHarness(formControlWrapperAncestor);
            const value = await input.getValue();
            expect(value).toBe('test me');
          });
        });

        describe('and user sets "  test me  " value', () => {
          beforeEach(async () => {
            const input = await getTextareaHarness(formControlWrapperAncestor);
            await input.focus();
            await input.setValue('  test me  ');
            await input.blur();
            fixture.detectChanges();
          });

          it('should update control value', async () => {
            const input = await getTextareaHarness(formControlWrapperAncestor);
            const value = await input.getValue();
            expect(value).toBe('test me');
          });
        });
      });

      describe('test input', () => {
        describe('and user sets "test me" value', () => {
          beforeEach(async () => {
            const input = await getInputHarness(formControlWrapperAncestor);
            await input.focus();
            await input.setValue('test me');
            await input.blur();
            fixture.detectChanges();
          });

          it('should not change control value', async () => {
            const input = await getInputHarness(formControlWrapperAncestor);
            const value = await input.getValue();
            expect(value).toBe('test me');
          });
        });

        describe('and user sets "  test me  " value', () => {
          beforeEach(async () => {
            const input = await getInputHarness(formControlWrapperAncestor);
            await input.focus();
            await input.setValue('  test me  ');
            await input.blur();
            fixture.detectChanges();
          });

          it('should update control value', async () => {
            const input = await getInputHarness(formControlWrapperAncestor);
            const value = await input.getValue();
            expect(value).toBe('test me');
          });
        });
      });
    })

    describe('test formControlName', () => {
      describe('test textarea', () => {
        describe('and user sets "test me" value', () => {
          beforeEach(async () => {
            const input = await getTextareaHarness(formControlNameWrapperAncestor);
            await input.focus();
            await input.setValue('test me');
            await input.blur();
            fixture.detectChanges();
          });

          it('should not change control value', async () => {
            const input = await getTextareaHarness(formControlNameWrapperAncestor);
            const value = await input.getValue();
            expect(value).toBe('test me');
          });
        });

        describe('and user sets "  test me  " value', () => {
          beforeEach(async () => {
            const input = await getTextareaHarness(formControlNameWrapperAncestor);
            await input.focus();
            await input.setValue('  test me  ');
            await input.blur();
            fixture.detectChanges();
          });

          it('should update control value', async () => {
            const input = await getTextareaHarness(formControlNameWrapperAncestor);
            const value = await input.getValue();
            expect(value).toBe('test me');
          });
        });
      });

      describe('test input', () => {
        describe('and user sets "test me" value', () => {
          beforeEach(async () => {
            const input = await getInputHarness(formControlNameWrapperAncestor);
            await input.focus();
            await input.setValue('test me');
            await input.blur();
            fixture.detectChanges();
          });

          it('should not change control value', async () => {
            const input = await getInputHarness(formControlNameWrapperAncestor);
            const value = await input.getValue();
            expect(value).toBe('test me');
          });
        });

        describe('and user sets "  test me  " value', () => {
          beforeEach(async () => {
            const input = await getInputHarness(formControlNameWrapperAncestor);
            await input.focus();
            await input.setValue('  test me  ');
            await input.blur();
            fixture.detectChanges();
          });

          it('should update control value', async () => {
            const input = await getInputHarness(formControlNameWrapperAncestor);
            const value = await input.getValue();
            expect(value).toBe('test me');
          });
        });
      });
    });
    
    describe('test elements without form control', () => {
      describe('test textarea', () => {
        describe('and user sets "test me" value', () => {
          beforeEach(async () => {
            const input = await getTextareaHarness(noControlWrapperAncestor);
            await input.focus();
            await input.setValue('test me');
            await input.blur();
            fixture.detectChanges();
          });

          it('should not change control value', async () => {
            const input = await getTextareaHarness(noControlWrapperAncestor);
            const value = await input.getValue();
            expect(value).toBe('test me');
          });
        });

        describe('and user sets "  test me  " value', () => {
          beforeEach(async () => {
            const input = await getTextareaHarness(noControlWrapperAncestor);
            await input.focus();
            await input.setValue('  test me  ');
            await input.blur();
            fixture.detectChanges();
          });

          it('should not change control value', async () => {
            const input = await getTextareaHarness(noControlWrapperAncestor);
            const value = await input.getValue();
            expect(value).toBe('  test me  ');
          });
        });
      });

      describe('test input', () => {
        describe('and user sets "test me" value', () => {
          beforeEach(async () => {
            const input = await getInputHarness(noControlWrapperAncestor);
            await input.focus();
            await input.setValue('test me');
            await input.blur();
            fixture.detectChanges();
          });

          it('should not change control value', async () => {
            const input = await getInputHarness(noControlWrapperAncestor);
            const value = await input.getValue();
            expect(value).toBe('test me');
          });
        });

        describe('and user sets "  test me  " value', () => {
          beforeEach(async () => {
            const input = await getInputHarness(noControlWrapperAncestor);
            await input.focus();
            await input.setValue('  test me  ');
            await input.blur();
            fixture.detectChanges();
          });

          it('should not change control value', async () => {
            const input = await getInputHarness(noControlWrapperAncestor);
            const value = await input.getValue();
            expect(value).toBe('  test me  ');
          });
        });
      });
    });


  });

  const getTextareaHarness = (ancestor: string): Promise<MatInputHarness> => {
    return loader.getHarness(
      MatInputHarness.with({
        ancestor,
      })
    );
  };

  const getInputHarness = (ancestor: string): Promise<MatInputHarness> => {
    return loader.getHarness(
      MatInputHarness.with({
        ancestor,
      })
    );
  };
});

const formControlWrapperAncestor = '.form-control-wrapper';
const formControlNameWrapperAncestor = '.form-control-name-wrapper';
const noControlWrapperAncestor = '.no-control-wrapper';

@Component({
  selector: 'app-test',
  template: `
    <div class="form-control-wrapper">
      <mat-form-field>
        <mat-label>Textarea formControl</mat-label>
        <textarea
          [formControl]="textAreaControl"
          appTrimOnBlur
          matInput
        ></textarea>
      </mat-form-field>

      <mat-form-field>
        <mat-label>Input formControl</mat-label>
        <input [formControl]="inputControl" appTrimOnBlur matInput/>
      </mat-form-field>
    </div>

    <div class="form-control-name-wrapper">
      <form [formGroup]="form">
        <mat-form-field>
          <mat-label>Textarea formControlName</mat-label>
          <textarea
            formControlName='textarea'
            appTrimOnBlur
            matInput
          ></textarea>
        </mat-form-field>

        <mat-form-field>
          <mat-label>Input formControlName</mat-label>
          <input formControlName='input' appTrimOnBlur matInput/>
        </mat-form-field>
      </form>
    </div>

    <div class="no-control-wrapper">
      <textarea appTrimOnBlur matInput></textarea>
      <input appTrimOnBlur matInput/>
    </div>
  `,
})
export class TestComponent {
  textAreaControl = new FormControl();
  inputControl = new FormControl();

  form = new FormGroup({
    input: new FormControl(),
    textarea: new FormControl(),
  });
}
