import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TrimOnBlurDirective } from './trim-on-blur.directive';


@NgModule({
  declarations: [
    TrimOnBlurDirective
  ],
  imports: [
    CommonModule
  ],
  exports: [
    TrimOnBlurDirective
  ]
})
export class TrimOnBlurModule {
}
